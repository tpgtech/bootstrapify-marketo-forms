# Bootstrapify Marketo Forms 2.0

Utilize the Bootstrap framework  with Marketo Forms 2.0.

## Getting Started
Download the [production version][min] or the [development version][max].

[min]: https://raw.github.com//bootstrapify/master/dist/mkto-bootstrapify.min.js
[max]: https://raw.github.com//bootstrapify/master/dist/mkto-bootstrapify.js

In your web page:

```html
<script src="jquery.js"></script>
<script src="dist/mkto-bootstrapify.min.js"></script>
<script>
jQuery(function($) {
  $.awesome(); // "awesome"
});
</script>
```

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Release History
_(Nothing yet)_
