/*! Bootstrapify Marketo Forms 2.0 - v0.1.0 - 2015-10-27
* Copyright (c) 2015 TPG Solution Engineering; Licensed MIT */
(function($) {

  // Collection method.
  $.fn.mkto_bootstrapify = function() {
    return this.each(function(i) {
      // Do something awesome to each selected element.
      $(this).html('awesome' + i);
    });
  };

  // Static method.
  $.mkto_bootstrapify = function(options) {
    // Override default options with passed-in options.
    options = $.extend({}, $.mkto_bootstrapify.options, options);
    // Return something awesome.
    return 'awesome' + options.punctuation;
  };

  // Static method default options.
  $.mkto_bootstrapify.options = {
    punctuation: '.'
  };

  // Custom selector.
  $.expr[':'].mkto_bootstrapify = function(elem) {
    // Is this element awesome?
    return $(elem).text().indexOf('awesome') !== -1;
  };

}(jQuery));
